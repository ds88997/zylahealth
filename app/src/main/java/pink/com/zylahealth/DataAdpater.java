package pink.com.zylahealth;


import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;



public class DataAdpater extends RecyclerView.Adapter<DataAdpater.ViewHolder> {

    private List<String> eventListResponses;
    private Context context;



    public DataAdpater(Context context, List<String> eventListResponses) {

        this.context = context;
        this.eventListResponses = eventListResponses;


    }

    @Override
    public int getItemCount() {
        return eventListResponses.size();
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.data_lay, parent, false);
        return new ViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.id.setText(eventListResponses.get(position));



    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id;



        public ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);



        }
    }
}
