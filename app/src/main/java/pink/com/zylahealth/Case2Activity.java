package pink.com.zylahealth;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Case2Activity extends AppCompatActivity {

    private RecyclerView list;
    private Call<DataHolder> call;
    private DataAdpater dataAdpater;
    private List<String> srt_list;
    private TextView load_page;
    private List<List<DataHolder.ItemsBean>> main_Data;
    private List<DataHolder.ItemsBean> data;
    private String href = "";
    private String next_page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case2);
        list = findViewById(R.id.list);
        load_page = findViewById(R.id.load_page);
        load_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                event_list(href, 2);
            }
        });
        main_Data = new ArrayList<>();
        srt_list = new ArrayList<>();
        LinearLayoutManager llm = new LinearLayoutManager(Case2Activity.this);
        llm.setStackFromEnd(true);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);


    }

    @Override
    protected void onResume() {
        super.onResume();


        event_list("", 1);


    }

    private void event_list(final String url, int param) {
        {
            // Utils.showProgress(getActivity(), "Please wait...");
            BaseInterface weatherCallInterface = Api.getClient().create(BaseInterface.class);

            if (param == 1) {

                call = weatherCallInterface.getUsers("address/pincode/");
            } else {

                call = weatherCallInterface.getUsers(url);


            }

            call.enqueue(new Callback<DataHolder>() {
                @Override
                public void onResponse(Call<DataHolder> call, final Response<DataHolder> response) {

                    // Utils.dismissProgress();
                    if (response.body() != null) {

                        data = response.body().getItems();
                        for (int i = 0; i < data.size(); i++) {
                            srt_list.add(data.get(i).get_id());
                        }
                        //  main_Data.add(data);

                        setAdapter(srt_list);
                        dataAdpater.notifyDataSetChanged();
                        href = response.body().getLinks().getNext().getHref();
                        String[] separated = href.split("=");
                       next_page=  separated[1];

                       load_page.setText("Fetch Page "+next_page);



                        Log.e("HREF", href);
                        //  Log.e("ITMES",no_items+"" );
                        // Log.e("CURRNT",current_page+"" );


                    }

                }

                @Override
                public void onFailure(Call<DataHolder> call, Throwable t) {
                    Log.e("image", "error" + t.getMessage());


                    // Utils.dismissProgress();


                }


            });

        }
    }

    private void setAdapter(List<String> data) {

        dataAdpater = new DataAdpater(Case2Activity.this, data);
        list.setAdapter(dataAdpater);

    }


}
