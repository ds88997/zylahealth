package pink.com.zylahealth;

import java.util.List;

public class DataHolder {

    /**
     * items : [{"_id":"5c189d14e296ea0001d1fcb1","pincode":744302,"state":"ANDAMAN & NICOBAR ISLANDS","district":"Nicobar","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb1"}}},{"_id":"5c189d14e296ea0001d1fcb2","pincode":744301,"state":"ANDAMAN & NICOBAR ISLANDS","district":"Nicobar","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb2"}}},{"_id":"5c189d14e296ea0001d1fcb3","pincode":744202,"state":"ANDAMAN & NICOBAR ISLANDS","district":"North And Middle Andaman","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb3"}}},{"_id":"5c189d14e296ea0001d1fcb4","pincode":744105,"state":"ANDAMAN & NICOBAR ISLANDS","district":"South Andaman","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb4"}}},{"_id":"5c189d14e296ea0001d1fcb5","pincode":744102,"state":"ANDAMAN & NICOBAR ISLANDS","district":"South Andaman","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb5"}}},{"_id":"5c189d14e296ea0001d1fcb6","pincode":744211,"state":"ANDAMAN & NICOBAR ISLANDS","district":"South Andaman","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb6"}}},{"_id":"5c189d14e296ea0001d1fcb7","pincode":744207,"state":"ANDAMAN & NICOBAR ISLANDS","district":"South Andaman","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb7"}}},{"_id":"5c189d14e296ea0001d1fcb8","pincode":744103,"state":"ANDAMAN & NICOBAR ISLANDS","district":"South Andaman","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb8"}}},{"_id":"5c189d14e296ea0001d1fcb9","pincode":744209,"state":"ANDAMAN & NICOBAR ISLANDS","district":"North And Middle Andaman","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb9"}}},{"_id":"5c189d14e296ea0001d1fcba","pincode":744304,"state":"ANDAMAN & NICOBAR ISLANDS","district":"Nicobar","_created":"Thu, 01 Jan 1970 00:00:00 GMT","_updated":"Thu, 01 Jan 1970 00:00:00 GMT","links":{"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcba"}}}]
     * links : {"parent":{"title":"home","href":"/"},"self":{"title":"address/pincode","href":"address/pincode?page=2"},"next":{"title":"next page","href":"address/pincode?page=3"},"last":{"title":"last page","href":"address/pincode?page=15557"},"prev":{"title":"previous page","href":"address/pincode"}}
     * meta : {"page":2,"max_results":10,"total":155570}
     */

    private LinksBean links;
    private MetaBean meta;
    private List<ItemsBean> items;

    public LinksBean getLinks() {
        return links;
    }

    public void setLinks(LinksBean links) {
        this.links = links;
    }

    public MetaBean getMeta() {
        return meta;
    }

    public void setMeta(MetaBean meta) {
        this.meta = meta;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class LinksBean {
        /**
         * parent : {"title":"home","href":"/"}
         * self : {"title":"address/pincode","href":"address/pincode?page=2"}
         * next : {"title":"next page","href":"address/pincode?page=3"}
         * last : {"title":"last page","href":"address/pincode?page=15557"}
         * prev : {"title":"previous page","href":"address/pincode"}
         */

        private ParentBean parent;
        private SelfBean self;
        private NextBean next;
        private LastBean last;
        private PrevBean prev;

        public ParentBean getParent() {
            return parent;
        }

        public void setParent(ParentBean parent) {
            this.parent = parent;
        }

        public SelfBean getSelf() {
            return self;
        }

        public void setSelf(SelfBean self) {
            this.self = self;
        }

        public NextBean getNext() {
            return next;
        }

        public void setNext(NextBean next) {
            this.next = next;
        }

        public LastBean getLast() {
            return last;
        }

        public void setLast(LastBean last) {
            this.last = last;
        }

        public PrevBean getPrev() {
            return prev;
        }

        public void setPrev(PrevBean prev) {
            this.prev = prev;
        }

        public static class ParentBean {
            /**
             * title : home
             * href : /
             */

            private String title;
            private String href;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class SelfBean {
            /**
             * title : address/pincode
             * href : address/pincode?page=2
             */

            private String title;
            private String href;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class NextBean {
            /**
             * title : next page
             * href : address/pincode?page=3
             */

            private String title;
            private String href;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class LastBean {
            /**
             * title : last page
             * href : address/pincode?page=15557
             */

            private String title;
            private String href;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class PrevBean {
            /**
             * title : previous page
             * href : address/pincode
             */

            private String title;
            private String href;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }
    }

    public static class MetaBean {
        /**
         * page : 2
         * max_results : 10
         * total : 155570
         */

        private int page;
        private int max_results;
        private int total;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getMax_results() {
            return max_results;
        }

        public void setMax_results(int max_results) {
            this.max_results = max_results;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }
    }

    public static class ItemsBean {
        /**
         * _id : 5c189d14e296ea0001d1fcb1
         * pincode : 744302
         * state : ANDAMAN & NICOBAR ISLANDS
         * district : Nicobar
         * _created : Thu, 01 Jan 1970 00:00:00 GMT
         * _updated : Thu, 01 Jan 1970 00:00:00 GMT
         * links : {"self":{"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb1"}}
         */

        private String _id;
        private int pincode;
        private String state;
        private String district;
        private String _created;
        private String _updated;
        private LinksBeanX links;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public int getPincode() {
            return pincode;
        }

        public void setPincode(int pincode) {
            this.pincode = pincode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String get_created() {
            return _created;
        }

        public void set_created(String _created) {
            this._created = _created;
        }

        public String get_updated() {
            return _updated;
        }

        public void set_updated(String _updated) {
            this._updated = _updated;
        }

        public LinksBeanX getLinks() {
            return links;
        }

        public void setLinks(LinksBeanX links) {
            this.links = links;
        }

        public static class LinksBeanX {
            /**
             * self : {"title":"Pincode","href":"address/pincode/5c189d14e296ea0001d1fcb1"}
             */

            private SelfBeanX self;

            public SelfBeanX getSelf() {
                return self;
            }

            public void setSelf(SelfBeanX self) {
                this.self = self;
            }

            public static class SelfBeanX {
                /**
                 * title : Pincode
                 * href : address/pincode/5c189d14e296ea0001d1fcb1
                 */

                private String title;
                private String href;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getHref() {
                    return href;
                }

                public void setHref(String href) {
                    this.href = href;
                }
            }
        }
    }
}
