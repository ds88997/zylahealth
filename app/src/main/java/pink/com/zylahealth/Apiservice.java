package pink.com.zylahealth;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;



    public interface Apiservice {
        @GET
        Call<DataHolder> getUsers(@Url String url);
    }

