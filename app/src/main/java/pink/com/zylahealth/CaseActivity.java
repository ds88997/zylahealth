package pink.com.zylahealth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CaseActivity extends AppCompatActivity {
    private Button case1,case2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case);
        case1=findViewById(R.id.case1);
        case2=findViewById(R.id.case2);
        case1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CaseActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

        case2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CaseActivity.this,Case2Activity.class);
                startActivity(intent);
            }
        });
    }
}
