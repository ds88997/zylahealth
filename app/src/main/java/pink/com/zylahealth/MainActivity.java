package pink.com.zylahealth;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView list;
    private Call<DataHolder> call;
    private DataAdpater dataAdpater;
    private TextView page_no, total_items;
    private int count = 2;
    private List<String> srt_list;
    private List<List<DataHolder.ItemsBean>> main_Data;
    private List<DataHolder.ItemsBean> data;
    private int current_page = 1, no_items;
    private Handler handler;
    private Runnable yourRunnable;
    private boolean b = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);
        page_no = findViewById(R.id.page_no);
        total_items = findViewById(R.id.total_items);
        main_Data = new ArrayList<>();
        srt_list = new ArrayList<>();
        handler = new Handler();
        LinearLayoutManager llm = new LinearLayoutManager(MainActivity.this);
        llm.setStackFromEnd(true);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);


    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.e("OnResume", "RUN");

        event_list("", 1);


    }

    private void event_list(final String url, int param) {
        if (b) {
            // Utils.showProgress(getActivity(), "Please wait...");
            BaseInterface weatherCallInterface = Api.getClient().create(BaseInterface.class);

            if (param == 1) {
                //  Log.e("Runs First Time", "True");
                call = weatherCallInterface.getUsers("address/pincode/");
            } else {
                // Log.e("Runs Seconds Time", "True");
                call = weatherCallInterface.getUsers(url);


            }

            call.enqueue(new Callback<DataHolder>() {
                @Override
                public void onResponse(Call<DataHolder> call, final Response<DataHolder> response) {

                    // Utils.dismissProgress();
                    if (response.body() != null) {

                        data = response.body().getItems();
                        for (int i = 0; i < data.size(); i++) {
                            srt_list.add(data.get(i).get_id());
                        }
                        //  main_Data.add(data);

                        setAdapter(srt_list);
                        dataAdpater.notifyDataSetChanged();
                        String href = response.body().getLinks().getNext().getHref();
                        current_page = response.body().getMeta().getPage();
                        no_items = no_items + data.size();
                        page_no.setText(current_page + "");
                        total_items.setText(no_items + "");
                        Log.e("HREF", href);
                        //  Log.e("ITMES",no_items+"" );
                        // Log.e("CURRNT",current_page+"" );


                        handler.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                event_list(response.body().getLinks().getNext().getHref(), count);


                            }
                        }, 500);


                    }


                }

                @Override
                public void onFailure(Call<DataHolder> call, Throwable t) {
                    Log.e("image", "error" + t.getMessage());


                    // Utils.dismissProgress();


                }


            });
        } else {
            handler.removeCallbacks(null);
        }


    }

    private void setAdapter(List<String> data) {

        dataAdpater = new DataAdpater(MainActivity.this, data);
        list.setAdapter(dataAdpater);

    }

    @Override
    public void onBackPressed() {
        b = false;
        Log.e("Backpress", "Called");
        finish();
        super.onBackPressed();
    }
}
